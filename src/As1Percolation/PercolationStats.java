package As1Percolation;

import edu.princeton.cs.introcs.StdRandom;
import edu.princeton.cs.introcs.StdStats;

/**
 * Created by bozkurti on 21/06/15.
 */

public class PercolationStats {
    public int N;
    public int T;
    public double fractionOfOpenSitesEachPercolation[];

    public PercolationStats(int N, int T) {    // perform T independent experiments on an N-by-N grid
        if (N <= 0 || T <= 0){ throw new IllegalArgumentException();}

        this.T = T;
        this.N = N;
        this.fractionOfOpenSitesEachPercolation = new double[T];
        int totalSites = N * N;

        for (int i = 0; i < T; i++) {
            Percolation per = new Percolation(N);
            int counterOfOpens = 0;
            do {
                int x = StdRandom.uniform(totalSites) % N + 1;//1 offset
                int y = StdRandom.uniform(totalSites) / N + 1;

                if (!per.isOpen(x, y))
                {//if it is not opened already then increment the counter
                    counterOfOpens++;
                }

                per.open(x, y);

            } while (!per.percolates());
            this.fractionOfOpenSitesEachPercolation[i] = (double)counterOfOpens / (double)totalSites;
        }
    }

    public double mean() {                     // sample mean of percolation threshold
        double mean = StdStats.mean(fractionOfOpenSitesEachPercolation);
        return mean;
    }

    public double stddev() {                    // sample standard deviation of percolation threshold
        double stdDev = StdStats.stddev(fractionOfOpenSitesEachPercolation);
        return stdDev;
    }

    public double confidenceLo() {             // low  endpoint of 95% confidence interval
        double mean = mean();
        double stdDevSqrt = Math.sqrt(stddev());

        double loCon = mean - (1.96 * stdDevSqrt / Math.sqrt(T));

        return loCon;
    }

    public double confidenceHi() {              // high endpoint of 95% confidence interval
        double mean = mean();
        double stdDev = stddev();

        double hiCon = mean + ((1.96*Math.sqrt(stdDev)) / Math.sqrt(T));

        return hiCon;
    }


    public static void main(String[] args) {   // test client (described below)
        PercolationStats perStats = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println("mean                    :" + perStats.mean());
        System.out.println("stddev                  :" + perStats.stddev());
        System.out.println("95% confidence interval :" + perStats.confidenceLo() + ", " + perStats.confidenceHi());
    }
}
