package As1Percolation;

import edu.princeton.cs.algs4.*;
import edu.princeton.cs.introcs.*;

/**
 * Created by bozkurti on 21/06/15.
*/

public class Percolation {

    static private final int kInvalidIndex = -1;

    private enum Neighbor{
        NeighborTop,
        NeighborBottom,
        NeighborLeft,
        NeighborRight
    }

    public int N;
    private WeightedQuickUnionUF quickUnion;
    private Boolean[] sites;        //holds the open or closed info

    public Percolation(int N)               // create N-by-N grid, with all sites blocked
    {
        if (N <= 0) {throw new IllegalArgumentException();}
        this.N = N;

        //1 for top, 1 for bottom lead sites.
        int sizeOfSites = N * N + 2;
        this.quickUnion = new WeightedQuickUnionUF(sizeOfSites);

        this.sites = new Boolean[sizeOfSites];      //defaults false
        for (int i = 0; i < sizeOfSites; i++){
            sites[i] = false;
        }
        this.sites[topSiteIndex()] = true;
        this.sites[bottomSiteIndex()] = true;
    }

    public void open(int i, int j)          // open site (row i, column j) if it is not open already
    {
        validateIndex(i, j);

        int privateIndex = privateIndex(i, j);
        sites[privateIndex] = true;

        int topNeighborIndex = neighborIndexOf(privateIndex, Neighbor.NeighborTop);
        int bottomNeighborIndex = neighborIndexOf(privateIndex, Neighbor.NeighborBottom);
        int leftNeighborIndex = neighborIndexOf(privateIndex, Neighbor.NeighborLeft);
        int rightNeighborIndex = neighborIndexOf(privateIndex, Neighbor.NeighborRight);

        if (topNeighborIndex != kInvalidIndex && sites[topNeighborIndex]){
            quickUnion.union(privateIndex, topNeighborIndex);
        }
        if (bottomNeighborIndex != kInvalidIndex && sites[bottomNeighborIndex]){
            quickUnion.union(privateIndex, bottomNeighborIndex);
        }
        if (leftNeighborIndex != kInvalidIndex && sites[leftNeighborIndex]){
            quickUnion.union(privateIndex, leftNeighborIndex);
        }
        if (rightNeighborIndex != kInvalidIndex && sites[rightNeighborIndex]){
            quickUnion.union(privateIndex, rightNeighborIndex);
        }
    }

    public boolean isOpen(int i, int j)     // is site (row i, column j) open?
    {
        validateIndex(i, j);

        return this.sites[privateIndex(i, j)];
    }

    public boolean isFull(int i, int j)     // is site (row i, column j) full?
    {
        validateIndex(i, j);

        int privateIndex = privateIndex(i, j);
        if (sites[privateIndex]){
            return quickUnion.connected(privateIndex, topSiteIndex());
        }
        else {
            return false;
        }
    }

    public boolean percolates()             // does the system percolate?
    {
        return quickUnion.connected(bottomSiteIndex(), topSiteIndex());
    }

    private int topSiteIndex(){
        return N * N;
    }

    private int bottomSiteIndex(){
        return N * N + 1;
    }

    private int privateIndex(int x, int y){
        return (x - 1) + ((y - 1) * N);
    }

    private void validateIndex(int x, int y){
        if (x <= 0 || x > N || y <= 0 || y > N){
            throw new IndexOutOfBoundsException();
        }
    }

    private int neighborIndexOf(int siteIndex, Neighbor neighborhood)
    {
        int x = siteIndex % N;
        int y = siteIndex / N;

        switch (neighborhood){
            case NeighborTop:{
                if (y == 0){
                    return this.topSiteIndex();
                }
                else{
                    y--;
                }
                break;
            }
            case NeighborBottom:{
                if (y == N - 1){
                    return this.bottomSiteIndex();
                }
                else{
                    y++;
                }
                break;
            }
            case NeighborLeft:{
                if (x == 0){
                    return kInvalidIndex;
                }
                else{
                    x--;
                }
                break;
            }
            case NeighborRight:{
                if (x == N - 1){
                    return kInvalidIndex;
                }
                else {
                    x++;
                }
                break;
            }
        }

        return x + (y * N);
    }


    public static void main(String[] args){
        int N = 3;

        Percolation per = new Percolation(N);

        Boolean doesPercolate = false;
        Boolean open = false;
        Boolean fullOpen = false;

        per.open(1, 1);
        per.open(1, 2);
        per.open(2, 2);
        per.open(2, 3);
        per.open(3, 3);

        open = per.isOpen(2, 2);
        open = per.isOpen(1, 1);
        open = per.isOpen(3, 2);        //false
        fullOpen = per.isFull(2, 2);
        fullOpen = per.isFull(2, 3);
        fullOpen = per.isFull(1, 3);    //false
        doesPercolate = per.percolates();
    }

}
